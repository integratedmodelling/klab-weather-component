/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.weather;

/**
 * Application path (default /opt/wmengine) can be changed using -Dwmengine.data.dir=<path>
 * Service port (default 8082) can be changed using -Dwmengine.port=<port>
 * 
 * Application path has the following subdirectories:
 * 
 *      ghcnd/      contains the GHCND database (ghcnd-inventory.txt, ghcnd-stations.txt, and all
 *                  station data filed under ghcnd-all/); if there, it's automatically indexed on
 *                  startup (long operation - takes several hours).
 *       data/      contains the station data after parsing of data files
 *      index/      contains the H2 index to all stations 
 *    contrib/      contains any CSV station definition file uploaded or contributed; these
 *                  are automatically parsed and added to the index if they parse OK. 
 * 
 * @author Ferd
 *
 */
public class Configuration {

    public static String        APPLICATION_PATH_SYSTEM_PROPERTY = "wmengine.data.dir";
    public static String        SERVICE_PORT_SYSTEM_PROPERTY     = "wmengine.port";

    private static final String DEFAULT_SERVICE_PORT             = "8028";
    private static final String DEFAULT_APPLICATION_PATH         = "/opt/wmengine";

    private static Integer      servicePort;
    private static String       applicationPath;

    public static String getApplicationPath() {
        if (applicationPath == null) {
            applicationPath = System.getProperty(APPLICATION_PATH_SYSTEM_PROPERTY);
            if (applicationPath /* still */ == null) {
                applicationPath = DEFAULT_APPLICATION_PATH;
            }
        }
        return applicationPath;
    }

    public static int getServicePort() {
        if (servicePort == null) {
            String sp = System.getProperty(SERVICE_PORT_SYSTEM_PROPERTY);
            servicePort = sp == null ? null : Integer.parseInt(sp);
            if (servicePort /* still */ == null) {
                servicePort = Integer.parseInt(DEFAULT_SERVICE_PORT);
            }
        }
        return servicePort;
    }

}
