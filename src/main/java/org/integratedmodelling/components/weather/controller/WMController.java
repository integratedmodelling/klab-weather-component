/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.components.weather.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.space.ShapeValue;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.components.weather.Configuration;
import org.integratedmodelling.components.weather.Endpoints;
import org.integratedmodelling.components.weather.resource.Capabilities;
import org.integratedmodelling.components.weather.resource.Station;
import org.integratedmodelling.components.weather.weather.Weather;
import org.integratedmodelling.components.weather.weather.WeatherFactory;
import org.integratedmodelling.components.weather.weather.WeatherKbox;
import org.integratedmodelling.components.weather.weather.WeatherStation;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class WMController {
    
    private static Log            logger  = LogFactory.getLog(WMController.class);

    public static boolean setup() {
        
        KLAB.setLogger(logger);
        
        /*
         * check database
         */
        try {
            WeatherFactory.get().checkDatabase();
        } catch (KlabException e) {
            return false;
        }

        return true;
    }

    @RequestMapping(value = Endpoints.CAPABILITIES, method = RequestMethod.GET)
    public Capabilities capabilities() {
        return new Capabilities();
    }

    /**
     * Return usable data between the passed start and end times. Accommodates various frequencies 
     * defaulting to daily.
     * 
     * @param bbox
     * @param variables
     * @return
     */
    @RequestMapping(Endpoints.GET_DATA)
    public List<Map<String, Object>> getStationData(
            @RequestParam(value = "bbox") String bbox,
            @RequestParam(value = "variables", defaultValue = "PRCP,TMAX,TMIN") String variables,
            @RequestParam(value = "start") Long start,
            @RequestParam(value = "end") Long end,
            @RequestParam(value = "step") Long step,
            @RequestParam(value = "max-years-back", defaultValue = "10") int maxYearsBack,
            @RequestParam(value = "max-nodata-percentage", defaultValue="23") int maxNodata,
            @RequestParam(value = "source", defaultValue="ALL") String source
            ) {
        
        List<Map<String, Object>> ret = new ArrayList<>();
        
        String[] vars  = null;
        if (!variables.isEmpty()) {
            List<String> vv = StringUtils.splitOnCommas(variables);
            vars = vv.toArray(new String[vv.size()]);
        }
                
        if (!bbox.isEmpty()) {

            double[] coords = StringUtils.splitToDoubles(bbox);
            if (coords.length != 4) {
                throw new KlabRuntimeException("wrong bbox specification: " + bbox);
            }
            
            ShapeValue shape = new ShapeValue(coords[0], coords[1], coords[2], coords[3]);
            try {
                Collection<WeatherStation> wss = WeatherKbox.get().within(shape, source, vars);
                Weather weather = new Weather(wss, start, end, step, maxYearsBack, vars, maxNodata, true);
                for (Map<String, Object> sd : weather.getStationData()) {
                    ret.add(sd);
                }
            } catch (KlabException e) {
                throw new KlabRuntimeException(e);
            }
            
        }
        return ret;
    }

    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public Station handleFileUpload(
            @RequestParam("name") String name,
            @RequestParam("file") MultipartFile file)  {
        
        File out = new File(Configuration.getApplicationPath() + File.separator + "contrib");
        out.mkdirs();
        out = new File(out + File.separator + name + "." 
                       + MiscUtilities.getFileExtension(file.getOriginalFilename()));
        
        if (!file.isEmpty()) {
            
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(out));
                stream.write(bytes);
                stream.close();

            } catch (Exception e) {
                throw new KlabRuntimeException(e);
            }
        } else {
            throw new KlabRuntimeException("file attachment was empty");
        }
        
        if (out.exists()) {
            try {
                WeatherStation ws = new WeatherStation(name, out);
                
                WeatherStation existing = WeatherKbox.get().retrieve(ws.getId());
                if (existing != null) {
                    logger.info("removing previously uploaded station " + ws.getId());
                    existing.remove();
                }
                WeatherKbox.get().store(ws);
                logger.info("successfully stored uploaded station " + ws.getId());
                return new Station(ws, true, null, null);
                
            } catch (KlabException e) {
                throw new KlabRuntimeException(e);
            }
        }
        
        return null;
    }
    
    /**
     * With default parameters, returns the list of stations matching the requirements with
     * minimal overhead. If include-data is true, also return all data in the passed range and
     * variables, or all data if not filtered.
     *  
     * @param lat
     * @param lon
     * @param range
     * @param bbox
     * @param variables
     * @param timeRange
     * @param verbose
     * @return
     * @throws  
     */
    @RequestMapping(value = Endpoints.GET_STATIONS, method = RequestMethod.GET)
    public List<Station> getStations(
            @RequestParam(value = "bbox") String bbox,
            @RequestParam(value = "variables", defaultValue = "") String variables,
            @RequestParam(value = "years", defaultValue = "") String timeRange,
            @RequestParam(value = "include-data", required = false) Boolean includeData,
            @RequestParam(value = "source", defaultValue="ALL") String source)  {
        
        List<Station> ret = new ArrayList<>();
        
        String[] vars  = null;
        if (!variables.isEmpty()) {
            List<String> vv = StringUtils.splitOnCommas(variables);
            vars = vv.toArray(new String[vv.size()]);
        }

        int[] years  = null;
        if (!timeRange.isEmpty()) {
            years = StringUtils.splitToIntegers(timeRange);
        }

        if (!bbox.isEmpty()) {

            double[] coords = StringUtils.splitToDoubles(bbox);
            if (coords.length != 4) {
                throw new KlabRuntimeException("wrong bbox specification: " + bbox);
            }
            
            ShapeValue shape = new ShapeValue(coords[0], coords[1], coords[2], coords[3]);
            try {
                for (WeatherStation ws : WeatherKbox.get().within(shape, source, vars)) {
                    /*
                     * if we have asked for data and specified both variables and years, only
                     * include stations that provide data.
                     */
                    Station station = new Station(ws, includeData != null && includeData, vars, years);
                    boolean ok = true;
                    if (includeData != null && includeData && (years != null || vars != null)) {
                        ok = !station.getData().isEmpty();
                    }
                    if (ok) {
                        ret.add(station);
                    }
                }
            } catch (KlabException e) {
                throw new KlabRuntimeException(e);
            }
            
        }
        
        return ret;
    }

    @RequestMapping(value = Endpoints.GET_STATION, method = RequestMethod.GET)
    public Station getStation(
            @RequestParam(value = "id") String id, 
            @RequestParam(value = "include-data",  required = false) Boolean includeData,
            @RequestParam(value = "variables", defaultValue = "") String variables,
            @RequestParam(value = "years", defaultValue = "") String timeRange) 
     {

        String[] vars  = null;
        if (!variables.isEmpty()) {
            List<String> vv = StringUtils.splitOnCommas(variables);
            vars = vv.toArray(new String[vv.size()]);
        }

        int[] years  = null;
        if (!timeRange.isEmpty()) {
            years = StringUtils.splitToIntegers(timeRange);
        }
        
        WeatherStation ws;
        try {
            ws = WeatherKbox.get().retrieve(id);
        } catch (KlabException e) {
            throw new KlabRuntimeException(e);
        }
        return ws == null ? null : new Station(ws, includeData == null ? false : includeData, vars, years);
    }
    
//    @ExceptionHandler(Exception.class)
//    public ModelAndView handleError(HttpServletRequest req, Exception exception) {
//      logger.error("Request: " + req.getRequestURL() + " raised " + exception);
//
//      ModelAndView mav = new ModelAndView();
//      mav.addObject("exception", exception);
//      mav.addObject("url", req.getRequestURL());
//      mav.setViewName("error");
//      return mav;
//    }
}
